# PRHawk

A web application that gives users visibility into how well GitHub repository owners are managing their Pull Requests.

## Getting Started

### Programming Language

This application was built with .Net Core 2.0 and React js.

### Prerequisites

To run this project we need .Net Core 2.0 and npm

* [Core 2.0](https://www.microsoft.com/net/download/) to install Core 2.0
* [npm](https://www.npmjs.com/get-npm) to install npm

### Build

To build PRHawk or PRHawk.Test, Open command prompt and browse to project folder and run :

```
dotnet restore && npm install

```

### Run

To run PRHawk, Open command prompt and browse to PRHawk directory.

```
dotnet run

```
Go to url, the application started listening to in the browser

To build this PRHawk.Test, Open command prompt and go to PRHawk.Test directory.

```
dotnet test

```