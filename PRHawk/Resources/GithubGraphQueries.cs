﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace PRHawk.Resources
{
    public class GithubGraphQueries
    {
        public static Func<string, string, string> GetUserPullRequestStatsQuery = (userName,after) => @"query{
                 user(login: " + ($"\"{userName}\"") + @"){
                    name,
                    bio,
                    email,
                    company,
                    avatarUrl,
                    repositories(privacy: PUBLIC, first: 100, isFork: false, " + (!string.IsNullOrEmpty(after) ? $"after:\"{after}\"" : "") + @"orderBy: { field: UPDATED_AT, direction: DESC}){
                        totalCount,
                        pageInfo{
                            hasNextPage,endCursor
                        },
                        edges{
                            node{
                                name,
                                description,
                                url,
                                pullRequests(states: OPEN){
                                    totalCount
                                }
                            }
                        }
                    }
                },
                rateLimit{cost,limit,nodeCount,remaining
                }
            }";
    }
}
