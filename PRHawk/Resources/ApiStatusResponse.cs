﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRHawk.Resources
{
    public class ApiStatusResponse
    {
        internal const string INTERNAL_SERVER_ERROR = "An internal exception has occurred. Please try again or contact admin";
        internal const string BAD_REQUEST = "Bad Request! Please try again!";
        internal const string GITHUB_API_ERROR = "Oops! Can't fetch data from github api. Please try again later!";
    }
}
