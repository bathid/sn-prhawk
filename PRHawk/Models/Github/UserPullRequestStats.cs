﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRHawk.Models.Github
{
    public class UserPullRequestStats
    {
        public UserDetails User { get; set; }
        public List<Repository> Repositories { get; set; }
        public PageInfo PageInfo { get; set; }
    }

    public class UserDetails
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public string Company { get; set; }
        public string AvatarUrl { get; set; }
    }

    public class Repository
    {
        public string Name { get; set; }
        public string GitHubUrl { get; set; }
        public string Description { get; set; }
        public int NumberOfPullRequests { get; set; }
    }

}
