﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRHawk.Models.Github
{
    public class PageInfo
    {
        public bool HasNextPage { get; set; }
        public string NextCursor { get; set; }
        public int TotalCount { get; set; }
    }
}
