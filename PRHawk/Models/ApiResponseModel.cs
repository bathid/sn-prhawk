﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRHawk.Models
{
    public class ApiResponseModel<T>
    {
        public T Data { get; set; }
        public RequestStatus Status { get; set; }      
    }

    public class RequestStatus
    {
        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
