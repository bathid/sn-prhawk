﻿import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import { GithubUser } from "./components/GithubUser";
import { NotFound } from "./components/NotFound";

export const routes = (
    <Layout>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/user/:username" component={GithubUser} />
            <Route path="*" component={NotFound} />
        </Switch>
    </Layout>

);

