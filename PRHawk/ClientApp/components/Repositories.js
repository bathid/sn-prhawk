import React, { Component } from "react";
import { RouteComponentProps } from "react-router";
import "isomorphic-fetch";

export class Repositories extends React.Component{
    constructor(props) {
        super(props);
      }

      render() {
        const status = this.props.TotalCount > 0?(
          <p>
            <em>{this.props.Repositories.length} of {this.props.TotalCount} repositories fetched</em>
          </p>
        ) : "";
    
        return (
          <div className="container-fluid">
              <div className="row text-right">{status}</div>
              <div className="row">{Repositories.renderRepositories(this.props.Repositories)}</div>
          </div>
        );
      }

      static renderRepositories(repositories) {
        return (
          <table className="table table-responsive">
            <thead>
              <tr>
                <th>Repository</th>
                <th className="text-center">Number of Pull Requests</th>
              </tr>
            </thead>
            <tbody>
              { (repositories.length > 0)?
                (repositories.sort((a, b) => b.NumberOfPullRequests - a.NumberOfPullRequests).map(repo => (
                    <tr key={repo.GitHubUrl} >
                    <td>
                        <h4><a href={repo.GitHubUrl} target="_github"> {repo.Name} </a></h4>
                        <h5>{repo.Description}</h5>
                    </td>
                    <td className="text-center text-weight-bold"><strong>{repo.NumberOfPullRequests}</strong></td>
                    </tr>))
                ):(
                    <tr>
                        <td colSpan="2" className="text-center">No public repositories found!</td>
                    </tr>
                )
            }
            </tbody>
          </table>
        );
      }
}