import React, { Component } from "react";
import { RouteComponentProps } from "react-router";
import { Redirect, RedirectProps } from "react-router-dom";

export class Search extends React.Component{
    state = {searchTerm:''}
    constructor(props){
        super(props);
        this.onSearchTermChange = this.onSearchTermChange.bind(this);
        this.onSearchSubmit = this.onSearchSubmit.bind(this);
    }

    onSearchTermChange(event){
        this.setState({searchTerm:event.target.value});
      }
    
    onSearchSubmit(event){
        event.preventDefault();
        if(this.state.searchTerm.trim() !== "" )
        {
           this.props.history.push("/user/"+this.state.searchTerm, null)
        }
        else
        this.setState({searchTerm:""});
      }
    
    render(){
        return (
            <div>
                <form className="navbar-form navbar-left col-sm-12" role="search"  onSubmit={this.onSearchSubmit}>
                <div className="input-group">
                    <input type="text" className="form-control" placeholder="Github username to search" onChange={this.onSearchTermChange}/>
                    <div className="input-group-btn">
                        <button className="btn btn-primary" type="submit">
                        <span className="glyphicon glyphicon-search"></span>
                        </button>
                    </div>
                </div>
                </form>  
            </div>         
        );
    }
    
}