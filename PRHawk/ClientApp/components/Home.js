﻿import React, { Component } from "react";
import { RouteComponentProps } from "react-router";

export class Home extends Component {
    render() {
        var style = {
            height:'500px',
            'margin-top':'10%',
            padding:'50px'
        };
        return (
        <div class="col-sm-12" style={style}>
                <h4><strong>PRHawk:</strong> a web application that gives users visibility into how well GitHub repository owners are managing their Pull Requests.</h4>
                <hr/>
                <div>
                 <h4>EndPoints:</h4>
                    <ul>
                        <li>
                            <p>
                                <h5><strong>user/:githubUserName</strong> You can use search bar in the header or directly navigate to the above end point</h5>                                
                                <ul>
                                    <li>Lists all the public repositories owned by the given user <strong>(Repositories that are forks are excluded)</strong> and order them in the descending order of their open pull requests.</li>
                                    <li><a href="https://developer.github.com/v4/" target="_blank">Github GraphQL Api V4</a> is used to fetch user repository and pull request details</li>
                                    <li>To Handle Github Api Pagination and to minimize number of requests from client to server, <strong>Server Sent Events</strong> are used. Server sends data as event-streams and client would listen to those events</li>
                                </ul>
                            </p>
                        </li>
                    </ul>
                </div>
        </div>
    );
}
}
