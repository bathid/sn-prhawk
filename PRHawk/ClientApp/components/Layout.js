import React, { Component } from "react";
import { NavMenu } from "./NavMenu";
import { RouteComponentProps } from "react-router";
import { Router, withRouter } from 'react-router-dom';

export class Layout extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <div className="container well wrapper">
        <header>
        <NavMenu/>
        </header>
        <div className="row">
          {this.props.children}
        </div>
      </div>
    );
  }
}
