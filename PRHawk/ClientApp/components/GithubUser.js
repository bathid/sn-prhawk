import React, { Component } from "react";
import { RouteComponentProps } from "react-router";
import "isomorphic-fetch";
import { timingSafeEqual } from "crypto";
import { Repositories } from "./Repositories";
import { UserDetails } from "./UserDetails";
import { HashLoader } from 'react-spinners';

export class GithubUser extends Component{
    state = { repositories: [], loading: true, userDetails : null, totalCount : 0};

    constructor(props) {
      super(props);
      this.eventSource = null;

      this.getData = this.getData.bind(this);
      this.onMessageReceived = this.onMessageReceived.bind(this);
      this.onConnectionError = this.onConnectionError.bind(this);
      this.onConnectionClosed = this.onConnectionClosed.bind(this);

      if(this.props.match.params.username.trim() !== ""){
        this.getData(this.props.match.params.username);
      }else{
        this.setState({loading:false});
      }
    }

    getData(userName) {
      this.eventSource = new EventSource("api/github/UserPullRequestStats/"+userName);

      this.eventSource.onmessage = this.onMessageReceived;
      this.eventSource.onerror = this.onConnectionError;
      this.eventSource.onclose = this.onConnectionClosed;
    }

    onMessageReceived = (event) => {
      var result = JSON.parse(event.data);
      var data = result.Data;
      if(result.Status.IsSuccess){              
          if(data.User != null && this.state.userDetails == null){
            this.setState({userDetails:data.User, loading:false});
          }

          if(data.Repositories.length > 0){
            var newRepos = data.Repositories;
            var oldRepos = [...this.state.repositories];
            if(oldRepos.length > 0){
              newRepos = [...oldRepos,...newRepos];
            }
            this.setState({repositories:newRepos, loading:false, totalCount : data.PageInfo.TotalCount});
  
            if(!data.PageInfo.HasNextPage){
              this.eventSource.close();
              return;
            }
          }
        }
        else if(this.state.loading){
          this.setState({loading:false});
      }else{
        this.eventSource.close();
      }
      return;
    };

    onConnectionError = () => {
      this.eventSource.close();
    };

    onConnectionClosed = () => {
      console.log("connection closed");
    };

    render() {
      const contents = this.state.loading ? "" : this.state.userDetails != null ? (
        <Repositories Repositories = {this.state.repositories} TotalCount={this.state.totalCount}></Repositories>
      ):"";
   
      return (
        <div>
          {(this.state.loading?(
            <div className='sweet-loading spinner-center'>
              <HashLoader
                color={'#000000'} 
                loading={this.state.loading} />
            </div>
            ):(
              <div className="col-sm-12">
                <UserDetails UserDetails = {this.state.userDetails} Username = {this.props.match.params.username}></UserDetails>
                <hr/>
                {contents}
              </div>
            ))}
        </div>
      );
    }
  }
  