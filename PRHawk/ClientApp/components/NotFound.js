﻿import React, { Component } from "react";
import { RouteComponentProps } from "react-router";


export class NotFound extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-sm-12 jumbotron">
                <p className="text-center">
                <strong> Oops! Requested resource not found.</strong>
                </p>
          </div>      
        );
    }
}
