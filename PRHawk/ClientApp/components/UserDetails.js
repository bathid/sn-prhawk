import React, { Component } from "react";
import { RouteComponentProps } from "react-router";


export class UserDetails extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const header = this.props.UserDetails != null ? (
            UserDetails.renderUserDetails(this.props.UserDetails)
          ):(
            <div className="row jumbotron message-center">
              <p className="text-center">
               <strong> Oops! Could not find user '{this.props.Username}'. Please try again!</strong>
              </p>
            </div>
          );

        return (
            <div>
                {header}
            </div>
        );
    }

    static renderUserDetails = (userDetails) => {
        return (
            <div className="row jumbotron">
                    <div className="col-sm-3 col-xs-12 text-center">
                        <img src={userDetails.AvatarUrl} width="180" height="180" />
                    </div>
                    <div className="col-sm-9 col-xs-12  text-left">
                        <h2 className="text-primary"><strong>{userDetails.Name}</strong></h2>
                        <table className="col-sm-6">
                        <tbody>
                            <tr>
                            <td>Email</td>
                            <td><strong>{userDetails.Email}</strong></td>
                            </tr>
                            <tr>
                            <td>Company</td>
                            <td><strong>{userDetails.Company}</strong></td>
                            </tr>
                            <tr>
                            <td>About Me</td>
                            <td><strong>{userDetails.Bio}</strong></td>
                            </tr>
                        </tbody>         
                        </table>
                    </div>
            </div>
        );
    };
}