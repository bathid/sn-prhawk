import React, { Component } from "react";
import { RouteComponentProps } from "react-router";
import { Link, NavLink, withRouter } from "react-router-dom";
import {Search} from "./Search";

export class NavMenu extends React.Component {
  
  constructor(props){
   super(props);
  }

  render() {
    var SearchWithRouter = withRouter(Search);

    return (
      <nav className="navbar navbar-inverse">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle"
              data-toggle="collapse"
              data-target=".navbar-collapse"
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <Link className="navbar-brand text-ligt" to={"/"}>
              PrHawk
            </Link>
          </div>
          <div className="navbar-collapse collapse pull-right">
            <SearchWithRouter />
          </div>      
        </nav>
    );
  }
}
