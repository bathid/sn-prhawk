﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRHawk.Extensions;
using PRHawk.Interfaces;
using PRHawk.Resources;

namespace PRHawk.Controllers
{
    [Route("api/[controller]")]
    public class GithubController : ControllerBase
    {
        private IGithubService _githubService;

        public GithubController(IGithubService githubService)
        {
            _githubService = githubService;
        }

        /// <summary>
        /// Returns the list of all the public repositories and their open pull request counts
        /// of a given github username as event streams to client.
        /// </summary>
        /// <param name="userName"></param>
        [HttpGet("userpullrequeststats/{userName}")]
        public async Task UserPullRequestStats(string userName)
        {
            Response.ContentType = "text/event-stream";

            if (string.IsNullOrEmpty(userName))
            {
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
            }
            else
            {
                var continueProcess = false;
                var afterCursor = "";
                do
                {
                    var responseObj = _githubService.GetUserPullRequestStats(userName, afterCursor).Result;

                    if (responseObj != null && (responseObj?.Data?.PageInfo.HasNextPage ?? false))
                    {
                        continueProcess = true;
                        afterCursor = responseObj?.Data?.PageInfo?.NextCursor ?? "";
                    }
                    else
                        continueProcess = false;

                    var jsonBody = JsonConvert.SerializeObject(responseObj);
                    await Response.WriteAsync($"data:{jsonBody}\n\n");
                    await Response.Body.FlushAsync();

                } while (continueProcess && !string.IsNullOrEmpty(afterCursor));
            }

            Response.Body.Close();
        }

    }
}