﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PRHawk.ServiceModels
{
    public class GitHubServiceResponseModel<T>
    {
        [JsonProperty(propertyName:"data")]
        [JsonConverter(typeof(JsonConvertObjectAsString))]
        public string Data { get; set; }
        [JsonProperty(propertyName: "errors")]
        public List<ErrorDetails> Errors { get; set; }
    }

    public class RateLimit
    {
        [JsonProperty(propertyName: "cost")]
        public int Cost { get; set; }
        [JsonProperty(propertyName: "limit")]
        public int Limit { get; set; }
        [JsonProperty(propertyName: "nodecount")]
        public int NodeCount { get; set; }
        [JsonProperty(propertyName: "remaining")]
        public int Remaining { get; set; }
    }

    public class ErrorDetails
    {
        [JsonProperty(propertyName: "message")]
        public string Message { get; set; }
        [JsonProperty(propertyName: "type")]
        public string Type { get; set; }
    }

    class JsonConvertObjectAsString : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(JTokenType));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var token = JToken.Load(reader);
            if (token.Type == JTokenType.Object)
                return token.ToString();
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var token = JToken.Parse(value.ToString());
            writer.WriteToken(token.CreateReader());
        }
    }
}
