﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PRHawk.Models;

namespace PRHawk.Extensions
{
    public static class RequestExtensions
    {
        public static ApiResponseModel<T> Success<T>(this HttpRequest request, HttpStatusCode statusCode, T data, string message)
        {
            return GetApiResponseObject<T>(true, statusCode, data, message);
        }

        public static ApiResponseModel<object> Error(this HttpRequest request, HttpStatusCode statusCode, string message)
        {
            return GetApiResponseObject<object>(false, statusCode, null, message);
        }

        private static ApiResponseModel<T> GetApiResponseObject<T>(bool isSuccess, HttpStatusCode statusCode, T data, string message)
        {
            return new ApiResponseModel<T>()
            {
                Data = data,
                Status = new RequestStatus()
                {
                    IsSuccess = isSuccess,
                    StatusCode = (int)statusCode,
                    Message = message
                }
            };

        }
    }
}
