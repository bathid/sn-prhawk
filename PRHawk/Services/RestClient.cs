﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using IRestClient = PRHawk.Interfaces.IRestClient;

namespace PRHawk.Services
{
    public class RestRequestClient:IRestClient
    {
        private RestClient _client;

        public bool ClientConfigured { get; set; }

        /// <summary>
        /// Configures the rest clients base url and basic authentication
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void ConfigureClient(string baseUrl, string userName, string password)
        {
            if (string.IsNullOrEmpty(baseUrl) || string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(password)) return;

            _client = new RestClient(baseUrl) {Authenticator = new HttpBasicAuthenticator(userName, password)};
            ClientConfigured = true;
        }
        /// <summary>
        /// Makes an Http Get request to given endpoint
        /// </summary>
        /// <param name="endPoint"></param>
        /// <param name="requestParams"></param>
        /// <returns></returns>
        public async Task<IRestResponse> Get(string endPoint, NameValueCollection requestParams)
        {
            if (!ClientConfigured) throw new Exception("Client not configured");

            try
            {
                var request = new RestRequest(endPoint, Method.GET);

                request.AddHeader("Accept", "application/json");

                if (requestParams?.HasKeys() ?? false)
                {
                    foreach (var key in requestParams.AllKeys)
                    {
                        request.AddQueryParameter(key, requestParams[key]);
                    }
                }

                return await _client.ExecuteTaskAsync(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Makes an Http Post request to the given endpoint with json body.
        /// </summary>
        /// <param name="endPoint"></param>
        /// <param name="data"></param>
        /// <param name="requestParams"></param>
        /// <returns></returns>
        public async Task<IRestResponse> Post(string endPoint, object data, NameValueCollection requestParams)
        {
            if (!ClientConfigured) throw new Exception("Client not configured");

            try
            {
                var request = new RestRequest(endPoint, Method.POST);

                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");

                if (requestParams?.HasKeys() ?? false)
                {
                    foreach (var key in requestParams.AllKeys)
                    {
                        request.AddQueryParameter(key, requestParams[key]);
                    }
                }

                if (data != null)
                    request.AddJsonBody(data);

                return await _client.ExecuteTaskAsync(request);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
