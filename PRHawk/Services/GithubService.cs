﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRHawk.Interfaces;
using PRHawk.Models;
using PRHawk.Models.Github;
using PRHawk.Resources;
using PRHawk.ServiceModels;

namespace PRHawk.Services
{
    public class GithubService : IGithubService
    {
        private IRestClient _client;
        private IConfiguration _configuration;

        private static string EndPoint => "/graphql ";

        public GithubService(IConfiguration configuration, IRestClient client)
        {
            _client = client;
            _configuration = configuration;
            var gitHubSection = _configuration.GetSection("GitHub");
            _client.ConfigureClient(gitHubSection["BaseUrl"],gitHubSection["UserName"], gitHubSection["Token"]);
        }

        /// <summary>
        /// Returns the list of 100 public repositories and their open pull request counts owned by the given username,
        /// paginitation is handled if the cursor is provided.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="afterCursor"></param>
        /// <returns></returns>
        public async Task<ApiResponseModel<UserPullRequestStats>> GetUserPullRequestStats(string userName, string afterCursor)
        {
           var response = new ApiResponseModel<UserPullRequestStats>()
           {
               Status = new RequestStatus()
               {
                   IsSuccess = false,
                   StatusCode = (int)HttpStatusCode.OK,
                   Message = ApiStatusResponse.GITHUB_API_ERROR
               }
           };

           var query = GithubGraphQueries.GetUserPullRequestStatsQuery(userName, afterCursor);

           var apiResponse = await _client.Post(EndPoint, new {query = query}, null);

            if (!apiResponse.IsSuccessful || apiResponse.StatusCode != HttpStatusCode.OK)
            {
                response.Status.StatusCode = (int) apiResponse.StatusCode;
                return response;
            }

            var responseObj = JsonConvert.DeserializeObject<GitHubServiceResponseModel<object>>(apiResponse.Content);

            if (responseObj == null || (responseObj.Errors?.Any() ?? false))
            {
                response.Status.Message = responseObj == null
                    ? ApiStatusResponse.GITHUB_API_ERROR
                    : string.Join(",", responseObj.Errors.Select(e => e.Message).ToArray());
            }
            else
            {
                var data = JObject.Parse(responseObj.Data);

                if (data == null)
                    return response;

                response.Data = new UserPullRequestStats
                {
                    User = new UserDetails()
                    {
                        Name = (data?["user"]?["name"] ?? "").ToString(),
                        Email = (data?["user"]?["email"] ?? "").ToString(),
                        Company = (data?["user"]?["company"] ?? "").ToString(),
                        AvatarUrl = (data?["user"]?["avatarUrl"] ?? "").ToString(),
                        Bio = (data?["user"]?["bio"] ?? "").ToString(),
                    }
                };

                var reps = (JArray)data["user"]["repositories"]["edges"];

                response.Data.Repositories = new List<Repository>();

                foreach (var rep in reps)
                {
                    var repositary = new Repository()
                    {
                        Name = (rep?["node"]?["name"]??"").ToString(),
                        Description = (rep?["node"]?["description"]??"").ToString(),
                        GitHubUrl = (rep?["node"]?["url"]??"").ToString(),
                        NumberOfPullRequests = int.Parse((rep?["node"]?["pullRequests"]?["totalCount"]??"0").ToString())
                    };

                    response.Data.Repositories.Add(repositary);
                }

                response.Data.PageInfo = new PageInfo()
                {
                    HasNextPage = bool.Parse((data?["user"]?["repositories"]?["pageInfo"]?["hasNextPage"]??"false").ToString()),
                    NextCursor = (data?["user"]?["repositories"]?["pageInfo"]?["endCursor"]??"").ToString(),
                    TotalCount = int.Parse((data?["user"]?["repositories"]?["totalCount"]??0).ToString())
                };

                response.Status.IsSuccess = true;
                response.Status.Message = "done";
            }

            return response;
        }
    }
}
