﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;

namespace PRHawk.Interfaces
{
    public interface IRestClient
    {
        void ConfigureClient(string baseUrl, string userName, string password);
        Task<IRestResponse> Get(string endPoint, NameValueCollection requestParams);
        Task<IRestResponse> Post(string endPoint, object data, NameValueCollection requestParams);
    }
}
