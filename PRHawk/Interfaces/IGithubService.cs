﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PRHawk.Models;
using PRHawk.Models.Github;

namespace PRHawk.Interfaces
{
    public interface IGithubService
    {
        Task<ApiResponseModel<UserPullRequestStats>> GetUserPullRequestStats(string userName, string after);
    }

}
