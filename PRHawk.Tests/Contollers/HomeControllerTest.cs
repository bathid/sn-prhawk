﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PRHawk.Controllers;
using Xunit;

namespace PRHawk.Tests.Contollers
{
    public class HomeControllerTest
    {
        private HomeController _controller;

        public HomeControllerTest()
        {
            _controller = new HomeController();
        }

        [Fact]
        public void IndexTest_ReturnsViewWithPlaceHolderForReactApp()
        {
            var result = _controller.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
        }
    }
}
