﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using PRHawk.Controllers;
using PRHawk.Interfaces;
using PRHawk.Models;
using PRHawk.Models.Github;
using Xunit;

namespace PRHawk.Tests.Contollers
{
    public class GithubControllerTest
    {
        private GithubController _controller;
        private Mock<IGithubService> _githubService;

        public GithubControllerTest()
        {
            _githubService = new Mock<IGithubService>();

            _controller = new GithubController(_githubService.Object)
            {
                ControllerContext = new ControllerContext {HttpContext = new DefaultHttpContext()}
            };
        }

        [Fact]
        public async Task UserPullRequestStatsTest_CheckResponseContentType()
        {
            var apiResponse = new ApiResponseModel<UserPullRequestStats>()
            {
                Data = new UserPullRequestStats()
                {
                    PageInfo = new PageInfo()
                    {
                        HasNextPage = false
                    }
                },
                Status = new RequestStatus()
                {
                    IsSuccess = true,
                    StatusCode = 200
                }
            };

            _githubService
                .Setup(s => s.GetUserPullRequestStats("test", ""))
                .Returns(Task.FromResult(apiResponse));

            await _controller.UserPullRequestStats("test");

            var response = _controller.ControllerContext.HttpContext.Response;

            Assert.Equal(200,response.StatusCode);
            Assert.Equal("text/event-stream",response.ContentType);
        }

        [Fact]
        public async Task UserPullRequestStatsTest_BadRequest()
        {

            await _controller.UserPullRequestStats("");

            var response = _controller.ControllerContext.HttpContext.Response;

            Assert.Equal(400, response.StatusCode);
        }


    }
}
