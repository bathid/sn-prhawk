﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Moq;
using PRHawk.Interfaces;
using PRHawk.Services;
using PRHawk.Tests.Extensions;
using Xunit;

namespace PRHawk.Tests.Services
{
    public class GitHubServiceTest
    {
        private GithubService _service;

        public GitHubServiceTest()
        {
            var configuration = BuildConfiguration.GetConfiguation();
            _service = new GithubService(configuration,new RestRequestClient());
        }

        [Fact]
        public async Task GetUserPullRequestStatsTest_ValidUserWithRepos()
        {
            var userName = "test";

            var response = await _service.GetUserPullRequestStats(userName,"");

            Assert.NotNull(response);
            Assert.Equal(200,response.Status.StatusCode);
            Assert.True(response.Status.IsSuccess);
            Assert.NotNull(response.Data);
            Assert.NotNull(response.Data.User);
            Assert.True(response.Data.Repositories.Any());
            Assert.Equal(33, response.Data.Repositories.Count);
        }

        [Fact]
        public async Task GetUserPullRequestStatsTest_ValidUserWithOutRepos()
        {
            var userName = "test23";

            var response = await _service.GetUserPullRequestStats(userName, "");

            Assert.NotNull(response);
            Assert.Equal(200, response.Status.StatusCode);
            Assert.True(response.Status.IsSuccess);
            Assert.NotNull(response.Data);
            Assert.NotNull(response.Data.User);
            Assert.True(!response.Data.Repositories.Any());
        }

        [Fact]
        public async Task GetUserPullRequestStatsTest_NotValidUser_1()
        {
            var userName = "baew422343b";

            var response = await _service.GetUserPullRequestStats(userName, "");

            Assert.NotNull(response);
            Assert.Null(response.Data);
            Assert.False(response.Status.IsSuccess);
            Assert.Equal(@"Could not resolve to a User with the login of 'baew422343b'.",response.Status.Message);
        }

        [Fact]
        public async Task GetUserPullRequestStatsTest_NotValidUser_2()
        {
            var userName = "";

            var response = await _service.GetUserPullRequestStats(userName, "");

            Assert.NotNull(response);
            Assert.False(response.Status.IsSuccess);
            Assert.Null(response.Data);
            Assert.Equal("Could not resolve to a User with the login of ''.",response.Status.Message);
        }
    }
}
